"use strict";
//  1. Типы данных в JS:
// - Number
// - String
// - Boolean
// - Object
// - BigInt
// - Symbol
// - null
// - undefined
// 2. Разница между == і === в том, что == являеться не строгим равенством, которое при сравнение значений, не берет во внимание типы данных, в то время как
// === являеться строгим равенством, и берет во внимание типы данных при сравнений значений.
// 3. Оператор - это символы в JS с помощью которых происходит сравнение, присвоение, математические вычисления и логические операторы.

let userName = prompt ('What is your name?');
while (userName === null || userName === "") {
    userName = prompt ('Please, write your name!')
}
// if (userName === null || userName === "") {
//     alert('Please, write your name!')
// }
let userAge = +prompt('How old are you?');
while (userAge === 0 || isNaN(userAge) || userAge === "") {
    userAge = +prompt ('Please, write your age!')
}
let result;

if (userAge < 18) {
    alert('You are not allowed to visit this website!')
} else if (userAge >= 18 && userAge <= 22) {
    result = confirm('Are you sure you want to continue?')
    if (result) {
        alert(`Welcome, ${userName}`)
    } else {
        alert('You are not allowed to visit this website!')
    }
} else /* if (userAge >= 23) */ {
    alert(`Welcome, ${userName}`)
}

console.log(`Your age: ${userAge}`);